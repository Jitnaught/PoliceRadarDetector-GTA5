﻿using GTA;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace PoliceRadar
{
    public class Main : Script
    {
        Model[] POLICE_VEH_MODELS =
        { //commented out models are cops which likely wouldn't have a radar irl
            //"FBI",
            //"FBI2",
            "Police",
            "Police2",
            "Police3",
            "Police4",
            "PoliceOld1",
            "PoliceOld2",
            //"PoliceT",
            "Policeb",
            "Pranger",
            "Predator",
            //"Riot",
            "Sheriff",
            "Sheriff2"
        };

        int speedLimit;
        float searchRadius;
        bool beepIfWanted;

        Stopwatch checkDistanceStopwatch = new Stopwatch(), getPoliceVehsStopwatch = new Stopwatch();

        List<Vehicle> nearbyPoliceVehicles = new List<Vehicle>();

        public Main()
        {
            speedLimit = Math.Max(0, Settings.GetValue("SETTINGS", "SPEED_LIMIT", 30));
            searchRadius = Math.Max(20f, Settings.GetValue("SETTINGS", "SEARCH_RADIUS", 250f));
            beepIfWanted = Settings.GetValue("SETTINGS", "BEEP_IF_HAVE_WANTED_LEVEL", false);

            Interval = 50;
            Tick += Main_Tick;
        }

        private void SoundNotExist_Tick(object sender, EventArgs e)
        {
            if (Game.IsLoading || Game.IsScreenFadedOut || Game.IsScreenFadingIn) return;

            UI.Notify("Error: The sound file for Police Radar was not found.");

            Tick -= SoundNotExist_Tick;
        }

        private void Main_Tick(object sender, EventArgs e)
        {
            if (Beep.errorNoSoundFile)
            {
                Tick -= Main_Tick;
                Interval = 500;
                Tick += SoundNotExist_Tick;
            }

            Player plr = Game.Player;
            Ped plrPed = plr.Character;

            if (plrPed != null && plrPed.Exists() && plrPed.IsAlive && plrPed.IsInVehicle())
            {
                Vehicle plrVeh = plrPed.CurrentVehicle;

                if (plrVeh != null && plrVeh.Exists() && plrVeh.IsAlive &&
                    (plrVeh.Model.IsBike || plrVeh.Model.IsBoat || plrVeh.Model.IsCar || plrVeh.Model.IsQuadbike) &&
                    (beepIfWanted || plr.WantedLevel == 0) && (speedLimit == 0 || plrVeh.Velocity.Length() > speedLimit))
                {
                    if (!getPoliceVehsStopwatch.IsRunning) getPoliceVehsStopwatch.Start();
                    else if (getPoliceVehsStopwatch.ElapsedMilliseconds >= 2500)
                    {
                        GetNearbyPoliceVehicles(plrPed);

                        getPoliceVehsStopwatch.Restart();
                    }

                    if (!checkDistanceStopwatch.IsRunning) checkDistanceStopwatch.Start();
                    else if (checkDistanceStopwatch.ElapsedMilliseconds >= 100)
                    {
                        float nearestDistance = searchRadius;

                        foreach (Vehicle veh in nearbyPoliceVehicles)
                        {
                            Yield();

                            float distance = Math.Max(10f, veh.Position.DistanceTo(plrPed.Position));

                            if (distance < nearestDistance) nearestDistance = distance;

                            if (distance == 10f) break;
                        }

                        if (nearestDistance < searchRadius)
                        {
                            Beep.beepInterval = (int)(nearestDistance * 10f);
                        }
                        else Beep.beepInterval = 0;

                        checkDistanceStopwatch.Restart();
                    }
                }
                else Stop();
            }
            else Stop();
        }

        private void Stop()
        {
            Beep.beepInterval = 0;

            if (getPoliceVehsStopwatch.IsRunning) getPoliceVehsStopwatch.Reset();

            if (checkDistanceStopwatch.IsRunning) checkDistanceStopwatch.Reset();
        }

        private void GetNearbyPoliceVehicles(Ped plrPed)
        {
            if (nearbyPoliceVehicles.Count != 0) nearbyPoliceVehicles.Clear();

            Yield();

            Vehicle[] nearbyVehicles = World.GetNearbyVehicles(plrPed, searchRadius);

            foreach (Vehicle veh in nearbyVehicles)
            {
                Yield();

                if (veh == null || !veh.Exists() || !veh.IsAlive || veh == plrPed.CurrentVehicle) continue;

                if (veh.IsSeatFree(VehicleSeat.Driver) && veh.IsSeatFree(VehicleSeat.Passenger)) continue;

                if (!POLICE_VEH_MODELS.Contains(veh.Model)) continue;

                nearbyPoliceVehicles.Add(veh);
            }
        }
    }
}
