﻿using GTA;
using System;
using System.Diagnostics;
using System.IO;
using System.Media;

namespace PoliceRadar
{
    class Beep : Script
    {
        const string SOUND_LOCATION = ".\\scripts\\PoliceRadarBeep.wav";

        public static int beepInterval = 0;
        public static bool errorNoSoundFile = false;

        Stopwatch beepStopwatch = new Stopwatch();
        SoundPlayer player;

        public Beep()
        {
            if (!File.Exists(SOUND_LOCATION))
            {
                errorNoSoundFile = true;
                return;
            }

            player = new SoundPlayer(SOUND_LOCATION);
            player.Load();

            Aborted += Beep_Aborted;
            Interval = 50;
            Tick += Beep_Tick;
        }

        private void Beep_Aborted(object sender, EventArgs e)
        {
            player.Stop();
            player.Dispose();
        }

        private void Beep_Tick(object sender, EventArgs e)
        {
            if (!beepStopwatch.IsRunning && beepInterval != 0) beepStopwatch.Start();
            else if (beepInterval != 0)
            {
                if (beepStopwatch.ElapsedMilliseconds >= beepInterval)
                {
                    player.Stop();

                    player.Play();

                    beepStopwatch.Restart();
                }
            }
            else beepStopwatch.Reset();
        }
    }
}
